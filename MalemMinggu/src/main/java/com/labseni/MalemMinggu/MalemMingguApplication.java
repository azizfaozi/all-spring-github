package com.labseni.MalemMinggu;

//Library dari sononya
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/* library nyontek dari google */
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;


@SpringBootApplication
public class MalemMingguApplication implements CommandLineRunner{
    private static final Logger log= LoggerFactory.getLogger(MalemMingguApplication.class);

    @Autowired
    private BookRepository repository;
    
    public static void main(String[] args) {
	SpringApplication.run(MalemMingguApplication.class, args);
    }

    @Override
    public void run(String... args){
	log.info("StartApplication...");
	repository.save(new Book("Java"));
	repository.save(new Book("Node"));
	repository.save(new Book("Python"));

	System.out.println("\nfindAll()");
	repository.findAll().forEach(x->System.out.println(x));

	System.out.println("\nfindAll()");
	repository.findById(1l).ifPresent(x->System.out.println(x));

	System.out.println("\nfindByName('Node')");
	repository.findByName("Node").forEach(x->System.out.println(x));

	

	
    }

}

